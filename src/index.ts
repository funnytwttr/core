import express, { Application } from 'express';

const app: Application = express();

const port: number = 3621;

app.get('/', (req, res) => {
    res.send('funny');
});

app.listen(port, () => {
    console.log("Now listening on %d", port);
});